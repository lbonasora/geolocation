//
//  LocationManager.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 08/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit
import UserNotifications

enum TrackingType {
    case Foreground, Background, Combined, None
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    static let sharedInstance = LocationManager()
    
    var delegate: CLLocationManagerDelegate?
    let manager = CLLocationManager()
    
    private override init() {
        super.init()
        
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = kCLLocationAccuracyNearestTenMeters
        requestPermission()  
    }
    
    func updateTrackingMethod(_ type: TrackingType) {
        switch type {
        case .Foreground:
            stopBackgroundTracking()
            beginForegroundTracking()
        case .Background:
            stopForegroundTracking()
            beginBackgroundTracking()
        case .Combined:
            beginBackgroundTracking()
            beginForegroundTracking()
        case .None:
            stopForegroundTracking()
            stopBackgroundTracking()
        }
    }
    
    private func requestPermission() {
        checkAuth(CLLocationManager.authorizationStatus())
    }
    
    private func stopBackgroundTracking() {
        manager.stopMonitoringSignificantLocationChanges()
    }
    
    private func beginBackgroundTracking() {
        manager.startMonitoringSignificantLocationChanges()
    }
    
    private func beginForegroundTracking() {
        manager.startUpdatingLocation()
    }
    
    private func stopForegroundTracking() {
        manager.stopUpdatingLocation()
    }
    
    private func checkAuth(_ status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways:
            //showAlert(title: "Tracking Authorized")
            print("📡 Tracking Authorized")
        case .authorizedWhenInUse:
            print("📡 Tracking authorized while using")
        case .denied:
            print("📡 Tracking denied, Abort")
        case .notDetermined:
            print("📡 Tracking Unknown, requesting permissions")
        case .restricted:
            print("📡 Tracking restricted, abort")
        }
    }
    
    //MARK - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuth(status)
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
        print("resume updates")
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
        print("Pause Updates")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Location Update \(locations)")
        for location in locations {
            decodeLocation(location)
        }
    }
    
    //Mark - Reverse Geocoding
    
    func decodeLocation (_ location: CLLocation) {
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location){ (placemark, error) in
            if error != nil {
                print("Reverse Geocoding failed with error: \(String(describing: error?.localizedDescription))")
            }
            
            let pms = placemark! as [CLPlacemark]
            
            if pms.count > 0 {
                let place = pms[0]
                print("Decoded Location: \(String(describing: place.addressDictionary))")
            }
        }
    }
}

extension LocationManager {
        
        public func monitorRegion(_ region: CLRegion) {
            manager.startMonitoring(for: region)
        }
        
        public func stopMonitoringRegion(_ region: CLRegion) {
            manager.stopMonitoring(for: region)
        }
        
        func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
            print("📡 Entered Regions \(region)")
            
            //Notificar al usuario
        }
        
        func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
            print("📡 Exit Region \(region)")
        }
}


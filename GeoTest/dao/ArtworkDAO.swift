//
//  ArtworkDAO.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 14/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import Alamofire

class ArtworkDAO {
    
    let endpoint = "https://data.honolulu.gov/resource/csir-pcj2.json"
    
    func getRemoteArtwork(termine: @escaping ([Artwork])->Void) -> Void {
        
        Alamofire.request(endpoint).responseJSON(completionHandler: { (myResponse) in
            
            if let value = myResponse.value as? [[String: AnyObject]] {
                var artworkArray: [Artwork] = []
                
                for artworkDictionary in value {
                    let artwork = Artwork(artWorkDict: artworkDictionary)
                    artworkArray.append(artwork)
                }
                termine(artworkArray)
            }
        })
    }
    
    func getArtworksFromFile() -> [Artwork]? {
        guard let fileName = Bundle.main.path(forResource: "PublicArt", ofType: "json")
            else { return nil}
        let optionalData = try? Data(contentsOf: URL(fileURLWithPath: fileName))
        
        guard
            let data = optionalData,
            // 2
            let json = try? JSONSerialization.jsonObject(with: data),
            // 3
            let dictionary = json as? [String: Any],
            // 4
            let works = dictionary["data"] as? [[Any]]
            else { return nil}
        // 5
        let validWorks = works.compactMap { Artwork(json: $0) }
        return validWorks
    }
    
}

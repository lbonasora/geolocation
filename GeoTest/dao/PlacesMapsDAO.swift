//
//  PlacesMapsDAO.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 24/8/18.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import Alamofire

struct PlacesApiConstants {
    static let ApiKey = "AIzaSyAefvVN7sbSXZXuASUWJcpw5GsDVQN_Bw8"
    static let baseURL = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?"
    static let inputtype = "textquery"
}

class PlacesMapsDAO {
    
    //api no disponible solo de tipo pago para accceder a info de places.
    
    func getPlacesPhotos(placeName: String, latitude: String, longitude: String, completion: @escaping (String) -> Void) {
        
        let placeFromNameUrl = PlacesApiConstants.baseURL
        var paramDic: [String: String] = [:]
        paramDic["key"] = PlacesApiConstants.ApiKey
        paramDic["input"] = placeName
        paramDic["inputtype"] = PlacesApiConstants.inputtype
        paramDic["point"] = "\(latitude), \(longitude)"
        paramDic["fields"] = "photos"
    }
    
}

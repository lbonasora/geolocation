//
//  ArtworkService.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 14/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import Alamofire

class ArtworkService {
    
    func getArtworksFromDAO(termine: @escaping ([Artwork])-> Void) -> Void {
        
        let artworkDAO = ArtworkDAO()
        
        artworkDAO.getRemoteArtwork(termine: { (artworks) in
            termine(artworks)
        })
    }
    
    func getArtworksFromDaoFile() -> [Artwork]? {
        
        let artworkDAO = ArtworkDAO()
        
        guard let artworks = artworkDAO.getArtworksFromFile() else {
            print("error getting arworks from file")
            return nil
        }
        
        return artworks
    }
}

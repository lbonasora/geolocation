//
//  Artwork.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 13/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import MapKit
import Contacts


class Artwork: NSObject, MKAnnotation {
    
    var locationName: String?
    var discipline: String?
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var latitude: String?
    var longitude: String?
    var artDescription: String?
    var year: String?
    var id: Int?
    var isFav: Bool = false
    
    init(artWorkDict: [String: AnyObject]) { //initializer to implement when API is available.
        
        self.locationName = artWorkDict[ArtworkConstants.locationNameKey] as? String
        self.discipline = artWorkDict[ArtworkConstants.disciplineKey] as? String
        self.title = artWorkDict[ArtworkConstants.titleKey] as? String
        self.latitude = artWorkDict[ArtworkConstants.latitude] as? String
        self.longitude = artWorkDict[ArtworkConstants.longitude] as? String
        
        
    
        
        if let latitude = self.latitude, let longitude = self.longitude {
            self.coordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude: Double(longitude)!)
        }  else {
            //parche para tratar optional
            self.coordinate = CLLocationCoordinate2D(latitude: 21.283921, longitude: -157.831661)
        }
        
        super.init()
    }
    
    init?(json: [Any]) {
        // 1
        self.title = json[16] as? String ?? ArtworkConstants.noTitle
        self.locationName = json[12] as? String
        self.discipline = json[15] as? String
        self.year = json[10] as? String
        self.artDescription = json[11] as? String ?? ArtworkConstants.noDescription
        self.id = json[0] as? Int
        // 2
        if let latitude = Double(json[18] as! String),
            let longitude = Double(json[19] as! String) {
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = CLLocationCoordinate2D()
        }
    }
    
    var subtitle: String? {
        return locationName
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }

}

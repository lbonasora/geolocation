//
//  User.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 23/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation
import Firebase

class User {
    var email: String?
    var name: String?
    var favoritesArtworks : [Artwork] = []
    var id: String?
    
    
    init() {
        if let user = Auth.auth().currentUser {
            self.email = user.email
            self.name = user.displayName
            self.id = user.uid
        }
    }
}



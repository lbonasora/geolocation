//
//  ViewController.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 06/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import CoreLocation

class ViewController: UIViewController, GIDSignInUIDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var signInButton: GIDSignInButton!
    
    var artworks: [Artwork] = []

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Google SignIn Button color theme
        GIDSignIn.sharedInstance().uiDelegate = self
        self.signInButton.colorScheme = GIDSignInButtonColorScheme(rawValue: 0)!
    }

    
    func getArtworks() {
        let artworkService = ArtworkService()
        artworkService.getArtworksFromDAO(termine: { (artworks) in
        
            self.artworks = artworks
        })
    }
    
    
}


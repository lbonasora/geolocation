//
//  MainViewController.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 07/08/2018.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MapKit

class MainViewController: UIViewController{
    
    var artworks : [Artwork] = []
    let regionRadius: CLLocationDistance = 1000
    let initialLocation = CLLocation(latitude: MainViewConstants.initialLatitude, longitude: MainViewConstants.initialLongitude)
    let ref = Database.database().reference()
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var leftNavBarButton: UIButton!
    var location: Artwork?
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.mapView.delegate = self
        self.mapView.showsUserLocation = true
        self.setUI()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            // Your code with delay
            self.centerMapOnLocation(location: self.initialLocation)
        }
        getArtworks()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getArtworks() {
        let artworkService = ArtworkService()
        
        if let artworks = artworkService.getArtworksFromDaoFile() {
            self.artworks = artworks
            mapView.addAnnotations(self.artworks)
        }
    }
    
    func setUI() {
        self.title = MainViewConstants.title
        self.leftNavBarButton.setTitle(MainViewConstants.signOutCTA, for: .normal)
    }
    
    @IBAction func signOut(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            if let nav = self.navigationController {
                nav.dismiss(animated: true)
            } else {
                dismiss(animated: true)
            }
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        let userLocation = mapView.userLocation
        
        if let coordinates = userLocation.location?.coordinate {
            let region = MKCoordinateRegion(center: coordinates, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
            mapView.setRegion(region, animated: true)
        }
        self.mapView.showsUserLocation = true
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
}

extension MainViewController: MKMapViewDelegate {
    // 1
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // 2
        guard let annotation = annotation as? Artwork else { return nil }
        // 3
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        // 4
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView,
                 calloutAccessoryControlTapped control: UIControl) {
        
        //push location to AnnotationDetail
        self.location = view.annotation as? Artwork
        
        performSegue(withIdentifier: SegueId.kSegueToArtworkDetail, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueId.kSegueToArtworkDetail {
            if let vc = segue.destination as? AnnotationDetailViewController,  let location = self.location {
                vc.location = location
            }
        }
    }

}


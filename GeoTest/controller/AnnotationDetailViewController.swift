//
//  AnnotationDetailViewController.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 24/8/18.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import UIKit
import Firebase
import MapKit

class AnnotationDetailViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var locationNameValueLabel: UILabel!
    @IBOutlet weak var disciplineLabel: UILabel!
    @IBOutlet weak var disciplineValueLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var yearValueLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    var location: Artwork?
    
    var ref: DatabaseReference!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
        if let description =  location?.id {
             print(description)
        }
               
        // Do any additional setup after loading the view.
    }

    @IBAction func directionsButtonWasTapped(_ sender: Any) {
        
        guard let locationSelected = self.location else {
           return
        }
        let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
        locationSelected.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    
    @IBAction func favButtonWasTapped(_ sender: Any) {
        ref = Database.database().reference()
        
        let user = Auth.auth().currentUser
        
        guard let locationSelected = self.location else {
            return
        }
        
        if locationSelected.isFav {
            
        }
        
        if let id = user?.uid, let artworkId = locationSelected.id {
            ref.child("users/\(id)/favoritesArtworks/" + String(artworkId)).setValue(true)
            self.location?.isFav = true
        }
     
    }
    
    func setUI() {
        
        guard let locationSelected = self.location, let title = locationSelected.title, let locationName = locationSelected.locationName, let description = locationSelected.artDescription, let year = locationSelected.year, let discipline = locationSelected.discipline else {
            return
        }
        
        self.titleLabel.text = title
        self.locationNameValueLabel.text = locationName
        self.descriptionText.text = description
        self.yearValueLabel.text = year
        self.disciplineValueLabel.text = discipline
        self.descriptionLabel.text = AnnotationDetailConstants.description
        self.disciplineLabel.text = AnnotationDetailConstants.discipline
        self.yearLabel.text = AnnotationDetailConstants.year
        self.locationNameLabel.text = AnnotationDetailConstants.locationName

    }
    

}

//
//  AppConstants.swift
//  GeoTest
//
//  Created by Lautaro Bonasora on 27/8/18.
//  Copyright © 2018 Lautaro Bonasora. All rights reserved.
//

import Foundation

struct ArtworkConstants {
    static let locationNameKey = "locationName"
    static let disciplineKey = "discipline"
    static let titleKey = "title"
    static let longitude = "longitude"
    static let latitude = "latitude"
    
    static let noDescription = "No Description"
    static let noTitle = "No Title"
}

struct AnnotationDetailConstants {
    static let locationName = "Location Name"
    static let discipline = "Discipline"
    static let year = "Year"
    static let description = "Description"
}

struct MainViewConstants {
    static let initialLatitude = 21.282778
    static let initialLongitude = -157.829444
    static let title = "Honolu Artworks"
    static let signOutCTA = "Sign Out"
}

struct SegueId {
    static let kSegueToArtworkDetail = "segueToArtworkDetail"
}
